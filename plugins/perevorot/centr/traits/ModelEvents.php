<?php

namespace Perevorot\Centr\Traits;

use October\Rain\Support\Facades\Str;

trait ModelEvents {

    public function beforeCreate()
    {
        if(empty($this->slug)) {
            if(isset($this->name)) {
                $this->slug = Str::slug($this->name);
            }
            elseif(isset($this->last_name)) {
                $this->slug = Str::slug($this->last_name);
            }
            elseif(isset($this->title)) {
                $this->slug = Str::slug($this->title);
            }
        }
    }

    public function beforeValidate()
    {
        if(empty($this->slug)) {
            if(isset($this->name)) {
                $this->slug = Str::slug($this->name);
            }
            elseif(isset($this->last_name)) {
                $this->slug = Str::slug($this->last_name);
            }
            elseif(isset($this->title)) {
                $this->slug = Str::slug($this->title);
            }
        }
    }
}