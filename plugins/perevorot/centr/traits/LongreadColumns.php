<?php namespace Perevorot\Centr\Traits;

use RainLab\Translate\Models\Locale;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

trait LongreadColumns
{
    public function createLongreadColumns(string $tableName)
    {
        $locales = Locale::where('is_enabled', true)->get();

        if (sizeof($locales) > 0) {
            foreach ($locales as $locale) {
                Schema::table($tableName, function(Blueprint $table) use ($locale, $tableName)
                    {
                    foreach(['longread_'] as $prefix)
                    {
                        $column = $prefix.$locale->code;

                        if (!Schema::hasColumn($tableName, $column))
                            $table->mediumText($column)->nullable();
                    }
                });
            }
        }
    }

    public function addLongreadColumns($form)
    {
    	$locales = Locale::isEnabled()->get();
        $fields = [];

        foreach ($locales as $locale) {
            $fields['longread_' . $locale->code] = [
                'type' => 'longread',
                'cssClass' => 'field-slim',
                'stretch' => true,
                'tab' => $locale->name
            ];
        }

        $form->addSecondaryTabFields($fields);
    }
}
