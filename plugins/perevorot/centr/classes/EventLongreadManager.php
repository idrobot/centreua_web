<?php namespace Perevorot\Centr\Classes;

use Event;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class EventLongreadManager
{
    /**
     * @var array
     */
    private $tables;

    /**
     * @param array $sideMenu
     */
    public function __construct()
    {
        
    }

    /**
     * @param array
     *
     * @return void
     */
    public function addTables(array $tables)    
    {
        $this->tables = $tables;
    }

    public function run()
    {
        foreach ($this->tables as $tableName) {
            Event::listen('eloquent.updated: RainLab\Translate\Models\Locale', function ($model) use ($tableName) {


                Schema::table($tableName, function (Blueprint $table) use ($model, $tableName) {
                    $column = 'longread_' . $model->code;

                    if (!Schema::hasColumn($tableName, $column)) {
                        $table->longText($column)->nullable();
                    }
                });
            });

            Event::listen('eloquent.saving: RainLab\Translate\Models\Locale', function ($model) use ($tableName) {


                Schema::table($tableName, function (Blueprint $table) use ($model, $tableName) {
                    $column = 'longread_' . $model->code;

                    if (!Schema::hasColumn($tableName, $column)) {
                        $table->longText($column)->nullable();
                    }
                });
            });

            Event::listen('eloquent.created: RainLab\Translate\Models\Locale', function ($model) use ($tableName) {
                Schema::table($tableName, function (Blueprint $table) use ($model, $tableName) {
                    $column = 'longread_' . $model->code;

                    if (!Schema::hasColumn($tableName, $column)) {
                        $table->longText($column)->nullable();
                    }
                });
            });

            Event::listen('eloquent.deleted: RainLab\Translate\Models\Locale', function ($model) use ($tableName) {
                Schema::table($tableName, function (Blueprint $table) use ($model, $tableName) {
                    $column = 'longread_' . $model->code;

                    if (Schema::hasColumn($tableName, $column)) {
                        $table->dropColumn($column);
                    }
                });
            });
        }
    }
}
