<?php namespace Perevorot\Centr\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Home extends Controller
{
    public $implement = [ 'Backend\Behaviors\ListController' ];
    
    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = [
        'centr.*' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Perevorot.Centr', 'centr', '');
    }
}
