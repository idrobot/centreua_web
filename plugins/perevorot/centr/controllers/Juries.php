<?php namespace Perevorot\Centr\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Juries Back-end Controller
 */
class Juries extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend\Behaviors\ReorderController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'centr.juries_manage'
    ];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Perevorot.Centr', 'contest', 'juries');
    }
}
