<?php namespace Perevorot\Centr\Controllers;

use BackendMenu;
use Backend\Widgets\Form;
use Backend\Classes\Controller;
use Perevorot\Centr\Traits\LongreadColumns;

/**
 * Contests Back-end Controller
 */
class Contests extends Controller
{
    use LongreadColumns;

    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = [
        'centr.contests_manage'
    ];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Perevorot.Centr', 'contest', 'contests');
    }

    public function formExtendFields(Form $form)
    {
        $this->addLongreadColumns($form);
    }
}
