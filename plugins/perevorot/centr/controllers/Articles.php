<?php namespace Perevorot\Centr\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use RainLab\Translate\Models\Locale;
use Backend\Widgets\Form;

class Articles extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'centr.articles' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Perevorot.Centr', 'publications', 'articles');
    }

    public function formExtendFields(Form $form)
    {
        $locales = Locale::isEnabled()->get();
        $fields = [];

        foreach ($locales as $locale) {
            $fields['longread_' . $locale->code] = [
                'type' => 'longread',
                'cssClass' => 'field-slim',
                'stretch' => true,
                'tab' => $locale->name,
            ];
        }

        $form->addTabFields($fields);
    }

    public function listExtendQuery($query)
    {
        $query->orderBy('created_at', 'desc');
    }
}
