<?php namespace Perevorot\Centr\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Participants Back-end Controller
 */
class Participants extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = [
        'centr.participants_manage'
    ];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Perevorot.Centr', 'contest', 'participants');
    }
}
