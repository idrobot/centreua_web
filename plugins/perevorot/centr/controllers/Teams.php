<?php namespace Perevorot\Centr\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Teams extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\ReorderController',
    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'centr.teams' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Perevorot.Centr', 'centr', 'teams');
    }

    public function listExtendQuery($query)
    {
        $query->orderBy('sort_order', 'asc');
    }
}
