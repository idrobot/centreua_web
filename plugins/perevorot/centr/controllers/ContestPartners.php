<?php namespace Perevorot\Centr\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Contest Partners Back-end Controller
 */
class ContestPartners extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend\Behaviors\ReorderController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'centr.contestpartners_manage'
    ];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Perevorot.Centr', 'contest', 'contestpartners');
    }
}
