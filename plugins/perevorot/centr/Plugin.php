<?php namespace Perevorot\Centr;

use Backend;
use System\Classes\PluginBase;
use Perevorot\Centr\Classes\EventLongreadManager;

/**
 * Centr Plugin Information File
 */
class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    public function register()
    {
        $this->registerConsoleCommand('thumbs.generate', 'Perevorot\Centr\Console\ThumbsGenerate');
    }

    public function boot()
    {
        $manager = new EventLongreadManager();

        $manager->addTables([
            'perevorot_centr_articles',
            'perevorot_centr_contests',
        ]);

        $manager->run();
    }
}
