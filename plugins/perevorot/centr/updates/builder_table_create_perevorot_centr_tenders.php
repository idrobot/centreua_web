<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePerevorotCentrTenders extends Migration
{
    public function up()
    {
        Schema::create('perevorot_centr_tenders', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 255)->nullable();
            $table->text('short')->nullable();
            $table->text('desc')->nullable();
            $table->integer('sort_order')->nullable()->unsigned()->default(1);
            $table->boolean('is_enabled')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('perevorot_centr_tenders');
    }
}
