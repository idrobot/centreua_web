<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotCentrTeams4 extends Migration
{
    public function up()
    {
        Schema::table('perevorot_centr_teams', function($table)
        {
            $table->string('facebook', 255)->nullable();
            $table->string('linkedin', 255)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_centr_teams', function($table)
        {
            $table->dropColumn('facebook');
            $table->dropColumn('linkedin');
        });
    }
}
