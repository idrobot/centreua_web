<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePerevorotCentrOrders extends Migration
{
    public function up()
    {
        Schema::create('perevorot_centr_orders', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('username');
            $table->integer('amount')->unsigned();
            $table->string('phone');
            $table->string('status');
            $table->text('response')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('perevorot_centr_orders');
    }
}
