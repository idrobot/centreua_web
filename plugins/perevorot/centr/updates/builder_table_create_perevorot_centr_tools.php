<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePerevorotCentrTools extends Migration
{
    public function up()
    {
        Schema::create('perevorot_centr_tools', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('link', 255);
            $table->integer('sort_order')->nullable()->unsigned()->default(1);
            $table->text('description')->nullable();
            $table->integer('page_id')->nullable()->unsigned();
            $table->boolean('is_enabled')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('perevorot_centr_tools');
    }
}
