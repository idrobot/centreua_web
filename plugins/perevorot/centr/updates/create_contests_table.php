<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateContestsTable extends Migration
{
    public function up()
    {
        Schema::create('perevorot_centr_contests', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('slug')->index()->unique();
            $table->text('description')->nullable();
            $table->boolean('is_main')->default(false);
            $table->boolean('is_enabled')->default(true);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('perevorot_centr_contests');
    }
}
