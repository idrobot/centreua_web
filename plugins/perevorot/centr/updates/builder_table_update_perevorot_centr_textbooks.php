<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotCentrTextbooks extends Migration
{
    public function up()
    {
        Schema::table('perevorot_centr_textbooks', function($table)
        {
            $table->integer('sort_order')->nullable()->unsigned()->default(1);
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_centr_textbooks', function($table)
        {
            $table->dropColumn('sort_order');
        });
    }
}
