<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotCentrSpecials extends Migration
{
    public function up()
    {
        Schema::table('perevorot_centr_specials', function($table)
        {
            $table->integer('page_id')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_centr_specials', function($table)
        {
            $table->dropColumn('page_id');
        });
    }
}
