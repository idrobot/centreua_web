<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotCentrTenders2 extends Migration
{
    public function up()
    {
        Schema::table('perevorot_centr_tenders', function($table)
        {
            $table->dropColumn('sort_order');
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_centr_tenders', function($table)
        {
            $table->integer('sort_order')->nullable()->unsigned()->default(1);
        });
    }
}
