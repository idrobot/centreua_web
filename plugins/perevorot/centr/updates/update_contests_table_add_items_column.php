<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateContestsTableAddItemsColumn extends Migration
{
    public function up()
    {
        Schema::table('perevorot_centr_contests', function($table)
        {
            $table->text('items')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_centr_contests', function($table)
        {
            $table->dropColumn('items');
        });
    }
}
