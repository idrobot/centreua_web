<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotCentrArticles extends Migration
{
    public function up()
    {
        Schema::table('perevorot_centr_articles', function($table)
        {
            $table->boolean('is_main')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_centr_articles', function($table)
        {
            $table->dropColumn('is_main');
        });
    }
}