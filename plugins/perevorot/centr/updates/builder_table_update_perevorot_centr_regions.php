<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotCentrRegions extends Migration
{
    public function up()
    {
        Schema::table('perevorot_centr_regions', function($table)
        {
            $table->integer('sort_order')->nullable()->unsigned();
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_centr_regions', function($table)
        {
            $table->dropColumn('sort_order');
            $table->increments('id')->unsigned()->change();
        });
    }
}
