<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePerevorotCentrPartners extends Migration
{
    public function up()
    {
        Schema::create('perevorot_centr_partners', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 255);
            $table->string('link', 255)->nullable();
            $table->integer('ptype_id')->nullable();
            $table->text('description')->nullable();
            $table->integer('sort_order')->nullable()->unsigned()->default(1);
            $table->boolean('is_enabled')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('perevorot_centr_partners');
    }
}
