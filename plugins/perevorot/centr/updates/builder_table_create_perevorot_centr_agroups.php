<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePerevorotCentrAgroups extends Migration
{
    public function up()
    {
        Schema::create('perevorot_centr_agroups', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 255);
            $table->boolean('is_enabled')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('perevorot_centr_agroups');
    }
}
