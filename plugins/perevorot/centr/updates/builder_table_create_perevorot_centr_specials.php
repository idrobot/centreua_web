<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePerevorotCentrSpecials extends Migration
{
    public function up()
    {
        Schema::create('perevorot_centr_specials', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('short')->nullable();
            $table->text('desc')->nullable();
            $table->integer('sort_order')->nullable()->unsigned()->default(1);
            $table->boolean('is_enabled')->nullable();
            $table->string('name');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('perevorot_centr_specials');
    }
}
