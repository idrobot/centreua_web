<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePerevorotCentrPtypes extends Migration
{
    public function up()
    {
        Schema::create('perevorot_centr_ptypes', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 255);
            $table->integer('sort_order')->nullable()->unsigned()->default(1);
            $table->boolean('is_enabled')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('perevorot_centr_ptypes');
    }
}
