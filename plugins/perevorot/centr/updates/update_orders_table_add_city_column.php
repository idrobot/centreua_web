<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateOrdersTableAddCityColumn extends Migration
{
    public function up()
    {
        Schema::table('perevorot_centr_orders', function($table)
        {
            $table->string('city')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_centr_orders', function($table)
        {
            $table->dropColumn('city');
        });
    }
}
