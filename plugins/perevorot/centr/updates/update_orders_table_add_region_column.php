<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateOrdersTableAddRegionColumn extends Migration
{
    public function up()
    {
        Schema::table('perevorot_centr_orders', function($table)
        {
            $table->string('email')->nullable();
            $table->integer('region_id')->nullable()->unsigned();
            $table->boolean('is_every_month')->default(false);
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_centr_orders', function($table)
        {
            $table->dropColumns([
                'email',
                'region_id'
            ]);
        });
    }
}
