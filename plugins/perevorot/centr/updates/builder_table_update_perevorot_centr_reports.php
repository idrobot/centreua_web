<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotCentrReports extends Migration
{
    public function up()
    {
        Schema::table('perevorot_centr_reports', function($table)
        {
            $table->text('description')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_centr_reports', function($table)
        {
            $table->dropColumn('description');
        });
    }
}
