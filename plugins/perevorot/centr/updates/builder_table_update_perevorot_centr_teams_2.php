<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotCentrTeams2 extends Migration
{
    public function up()
    {
        Schema::table('perevorot_centr_teams', function($table)
        {
            $table->integer('sort_order')->nullable()->unsigned()->default(1);
            $table->dropColumn('position');
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_centr_teams', function($table)
        {
            $table->dropColumn('sort_order');
            $table->string('position', 255)->nullable();
        });
    }
}
