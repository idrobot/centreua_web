<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotCentrSpecials2 extends Migration
{
    public function up()
    {
        Schema::table('perevorot_centr_specials', function($table)
        {
            $table->string('url')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_centr_specials', function($table)
        {
            $table->dropColumn('url');
        });
    }
}
