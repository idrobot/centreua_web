<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePerevorotCentrSubscriptions extends Migration
{
    public function up()
    {
        Schema::create('perevorot_centr_subscriptions', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('email', 255);
            $table->timestamp('created_at');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('perevorot_centr_subscriptions');
    }
}
