<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotCentrGroups extends Migration
{
    public function up()
    {
        Schema::table('perevorot_centr_groups', function($table)
        {
            $table->integer('sort_order')->nullable()->unsigned()->default(1);
            $table->increments('id')->unsigned(false)->change();
            $table->dropColumn('position');
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_centr_groups', function($table)
        {
            $table->dropColumn('sort_order');
            $table->increments('id')->unsigned()->change();
            $table->string('position', 255)->nullable();
        });
    }
}
