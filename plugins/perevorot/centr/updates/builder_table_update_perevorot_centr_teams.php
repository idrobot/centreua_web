<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotCentrTeams extends Migration
{
    public function up()
    {
        Schema::table('perevorot_centr_teams', function($table)
        {
            $table->string('full_name', 255)->nullable();
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_centr_teams', function($table)
        {
            $table->dropColumn('full_name');
            $table->increments('id')->unsigned()->change();
        });
    }
}
