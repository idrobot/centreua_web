<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotCentrTeams3 extends Migration
{
    public function up()
    {
        Schema::table('perevorot_centr_teams', function($table)
        {
            $table->string('position', 255)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_centr_teams', function($table)
        {
            $table->dropColumn('position');
        });
    }
}
