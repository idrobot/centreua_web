<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotCentrArticles4 extends Migration
{
    public function up()
    {
        Schema::table('perevorot_centr_articles', function($table)
        {
            $table->string('copyright')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_centr_articles', function($table)
        {
            $table->dropColumn('copyright');
        });
    }
}
