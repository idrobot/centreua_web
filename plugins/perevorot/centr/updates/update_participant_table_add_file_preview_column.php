<?php namespace Perevorot\Centr\Updates;

use DB;
use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateParticipantTableAddFilePreviewColumn extends Migration
{
    public function up()
    {
        Schema::table('perevorot_centr_participants', function($table)
        {
            if (!Schema::hasColumn('perevorot_centr_participants', 'file_preview')) {
                 DB::statement("ALTER TABLE perevorot_centr_participants ADD file_preview MEDIUMBLOB");
            }
        });
    }
    
    public function down()
    {
        if (Schema::hasTable('perevorot_centr_participants')) {
            if (Schema::hasColumn('perevorot_centr_participants', 'file_preview')) {
                Schema::table('perevorot_centr_participants', function($table)
                {
                     $table->dropColumn('file_preview');
                });
            }
        }
    }
}
