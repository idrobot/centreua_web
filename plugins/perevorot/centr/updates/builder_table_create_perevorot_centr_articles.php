<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePerevorotCentrArticles extends Migration
{
    public function up()
    {
        Schema::create('perevorot_centr_articles', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title', 255);
            $table->text('desc')->nullable();
            $table->text('short')->nullable();
            $table->text('tags')->nullable();
            $table->integer('agroup_id')->nullable()->unsigned();
            $table->integer('page_id')->nullable()->unsigned();
            $table->boolean('is_enabled')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('perevorot_centr_articles');
    }
}
