<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateOrdersTableUpdateEmailColumn extends Migration
{
    public function up()
    {
        Schema::table('perevorot_centr_orders', function($table)
        {
            $table->string('phone')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_centr_orders', function($table)
        {
            $table->string('phone')->change();
        });
    }
}
