<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateContestPartnersTable extends Migration
{
    public function up()
    {
        Schema::create('perevorot_centr_contest_partners', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('slug')->index()->unique();
            $table->string('link')->nullable();
            $table->boolean('is_enabled')->default(true)->index();
            $table->integer('sort_order')->unsigned()->index()->default(1);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('perevorot_centr_contest_partners');
    }
}
