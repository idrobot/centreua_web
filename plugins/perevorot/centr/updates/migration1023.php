<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration1023 extends Migration
{
    public function up()
    {
        Schema::table('perevorot_centr_articles', function($table)
        {
            if(!Schema::hasColumn('perevorot_centr_articles', 'longread_ua')) {
                $table->longText('longread_ua')->nullable();
            }
            if(!Schema::hasColumn('perevorot_centr_articles', 'longread_en')) {
                $table->longText('longread_en')->nullable();
            }
        });
    }

    public function down()
    {
        Schema::table('perevorot_centr_articles', function($table)
        {
            if(Schema::hasColumn('perevorot_centr_articles', 'longread_ua')) {
                $table->dropColumn('longread_ua');
            }
            if(Schema::hasColumn('perevorot_centr_articles', 'longread_en')) {
                $table->dropColumn('longread_en');
            }
        });
    }
}