<?php namespace Perevorot\Centr\Updates;

use DB;
use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateParticipantTableInlineAddFileColumn extends Migration
{
    public function up()
    {
        Schema::table('perevorot_centr_participants', function($table)
        {
            if (Schema::hasColumn('perevorot_centr_participants', 'file')) {
                 $table->dropColumn('file');
            }

            if (Schema::hasColumn('perevorot_centr_participants', 'file_inline')) {
                 $table->dropColumn('file_inline');
            }
        });

        Schema::table('perevorot_centr_participants', function($table)
        {
            if (!Schema::hasColumn('perevorot_centr_participants', 'file')) {
                 DB::statement("ALTER TABLE perevorot_centr_participants ADD file MEDIUMBLOB");

            }

            if (!Schema::hasColumn('perevorot_centr_participants', 'file_inline')) {
                 DB::statement("ALTER TABLE perevorot_centr_participants ADD file_inline MEDIUMBLOB");
            }
        });
    }
    
    public function down()
    {
        if (Schema::hasTable('perevorot_centr_participants')) {
            if (Schema::hasColumn('perevorot_centr_participants', 'file')) {
                Schema::table('perevorot_centr_participants', function($table)
                {
                     $table->dropColumn('file');
                });
            }

            if (Schema::hasColumn('perevorot_centr_participants', 'file_inline')) {
                Schema::table('perevorot_centr_participants', function($table)
                {
                     $table->dropColumn('file_inline');
                });
            }
        }
    }
}
