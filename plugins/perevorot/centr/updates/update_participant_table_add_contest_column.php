<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateParticipantTableAddContestColumn extends Migration
{
    public function up()
    {
        Schema::table('perevorot_centr_participants', function($table)
        {
            $table->integer('contest_id')->nullable();
        })
;    }
    
    public function down()
    {
        if (Schema::hasTable('perevorot_centr_participants')) {
            if (Schema::hasColumn('perevorot_centr_participants', 'contest_id')) {
                Schema::table('perevorot_centr_participants', function($table)
                {
                     $table->dropColumn('contest_id');
                });
            }
        }
    }
}
