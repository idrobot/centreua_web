<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateParticipantsTable extends Migration
{
    public function up()
    {
        Schema::create('perevorot_centr_participants', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('full_name')->nullable();
            $table->date('birthday')->nullable();
            $table->string('work_place')->nullable();
            $table->string('position')->nullable();
            $table->string('region')->nullable();
            $table->string('city')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('facebook_link')->nullable();
            $table->text('about_work')->nullable();
            $table->boolean('is_confirm')->default(false);
            $table->boolean('is_acquainted')->default(false);
            $table->boolean('is_verify')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('perevorot_centr_participants');
    }
}
