<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotCentrTenders extends Migration
{
    public function up()
    {
        Schema::table('perevorot_centr_tenders', function($table)
        {
            $table->dateTime('dt')->nullable();
            $table->string('city')->nullable();
            $table->string('slug');
            $table->string('name', 255)->nullable(false)->change();
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_centr_tenders', function($table)
        {
            $table->dropColumn('dt');
            $table->dropColumn('city');
            $table->dropColumn('slug');
            $table->string('name', 255)->nullable()->change();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
}
