<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePerevorotCentrTextbooks extends Migration
{
    public function up()
    {
        Schema::create('perevorot_centr_textbooks', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('year');
            $table->string('name');
            $table->text('short')->nullable();
            $table->boolean('is_enabled')->nullable()->default(0);
            $table->integer('page_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('perevorot_centr_textbooks');
    }
}
