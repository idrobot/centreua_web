<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePerevorotCentrTeams extends Migration
{
    public function up()
    {
        Schema::create('perevorot_centr_teams', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('description')->nullable();
            $table->string('email', 255)->nullable();
            $table->string('position', 255)->nullable();
            $table->integer('group_id')->nullable()->unsigned();
            $table->integer('region_id')->nullable()->unsigned();
            $table->boolean('is_enabled')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('perevorot_centr_teams');
    }
}
