<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateContestsTable extends Migration
{
    public function up()
    {
        Schema::table('perevorot_centr_contests', function($table)
        {
            $table->string('back_url')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_centr_contests', function($table)
        {
            $table->dropColumn('back_url');
        });
    }
}
