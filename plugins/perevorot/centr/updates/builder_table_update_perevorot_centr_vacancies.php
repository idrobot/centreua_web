<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotCentrVacancies extends Migration
{
    public function up()
    {
        Schema::table('perevorot_centr_vacancies', function($table)
        {
            $table->dateTime('dt')->nullable();
            $table->string('city')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_centr_vacancies', function($table)
        {
            $table->dropColumn('dt');
            $table->dropColumn('city');
        });
    }
}
