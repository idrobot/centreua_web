<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration1021 extends Migration
{
    public function up()
    {
        Schema::table('perevorot_centr_articles', function($table)
        {
            $table->index('agroup_id');
            $table->index('page_id');
        });
    }

    public function down()
    {
        Schema::table('perevorot_centr_articles', function($table)
        {
            $table->dropIndex(['agroup_id']);
            $table->dropIndex(['page_id']);
        });
    }
}