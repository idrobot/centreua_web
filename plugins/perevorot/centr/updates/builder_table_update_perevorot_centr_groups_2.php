<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdatePerevorotCentrGroups2 extends Migration
{
    public function up()
    {
        Schema::table('perevorot_centr_groups', function($table)
        {
            $table->string('slug', 255);
        });
    }
    
    public function down()
    {
        Schema::table('perevorot_centr_groups', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}
