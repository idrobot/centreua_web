<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePerevorotCentrTagsToArticles extends Migration
{
    public function up()
    {
        Schema::create('perevorot_centr_tags_to_articles', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('tag_id')->unsigned()->index();
            $table->integer('article_id')->unsigned()->index();

            $table->foreign('tag_id')->references('id')->on('perevorot_centr_tags')->onDelete('cascade');
            $table->foreign('article_id')->references('id')->on('perevorot_centr_articles')->onDelete('cascade');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('perevorot_centr_tags_to_articles');
    }
}