<?php namespace Perevorot\Centr\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateJuriesTable extends Migration
{
    public function up()
    {
        Schema::create('perevorot_centr_juries', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('full_name')->nullable();
            $table->string('slug')->index()->unique();
            $table->string('position')->nullable();
            $table->string('company')->nullable();
            $table->text('text')->nullable();
            $table->boolean('is_main')->default(false);
            $table->boolean('is_enabled')->default(true)->index();
            $table->integer('sort_order')->unsigned()->index()->default(1);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('perevorot_centr_juries');
    }
}
