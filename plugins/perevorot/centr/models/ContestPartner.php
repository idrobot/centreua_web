<?php namespace Perevorot\Centr\Models;

use Model;
use October\Rain\Database\Traits\Sortable;
use October\Rain\Database\Traits\Validation;

/**
 * ContestPartner Model
 */
class ContestPartner extends Model
{
    use Sortable, Validation;

    public $implement = [
        '@RainLab.Translate.Behaviors.TranslatableModel',
    ];

    public $translatable = [
        ['name', 'index'=>true]
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'perevorot_centr_contest_partners';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required',
        'slug' => 'required|unique:perevorot_centr_contest_partners',
        'link' => 'required|url',
        'logo' => 'required',
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'logo' => 'System\Models\File'
    ];
    public $attachMany = [];

    public function getThumbNameAttribute()
    {
        if (!$this->logo) {
            return $this->name;
        }

        echo "<img src='{$this->logo->path}' width='50'><span style='padding: 20px;'>$this->name}</span>";
    }
}
