<?php namespace Perevorot\Centr\Models;

use Illuminate\Support\Facades\Storage;
use Model;

/**
 * Model
 */
class Partner extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $implement = [
        '@RainLab.Translate.Behaviors.TranslatableModel',
    ];

    public $translatable = [
        ['name', 'index'=>true],
        ['description', 'index'=>true]
    ];
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required',
        'sort_order' => 'required',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'perevorot_centr_partners';

    public $attachOne = [
        'image' => ['System\Models\File'],
    ];

    public function getPtypeIdOptions()
    {
        return Ptype::all()->lists('name', 'id');
    }

    public function beforeSave()
    {
        $image=$this->image()->withDeferred(post('_session_key'))->first();

        if($image && file_exists($image->getLocalPath())) {

            Storage::disk('media')->put($image->file_name, file_get_contents($image->getLocalPath()));

            $image->getThumb(180, 110, [
                'mode' => 'auto',
                'quality' => 90
            ]);
        }
    }
}
