<?php namespace Perevorot\Centr\Models;

use Illuminate\Support\Facades\Storage;
use Model;
use Perevorot\Page\Models\Page;

/**
 * Model
 */
class Special extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $implement = [
        '@RainLab.Translate.Behaviors.TranslatableModel',
    ];

    public $translatable = [
        ['name', 'index'=>true],
        ['short', 'index'=>true],
        ['desc', 'index'=>true]
    ];
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required',
        'sort_order' => 'required',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'perevorot_centr_specials';

    public $attachOne = [
        'image' => ['System\Models\File'],
    ];

    public $hasOne = [
        'page' => ['Perevorot\Page\Models\Page',
            'key' => 'id',
            'otherKey' => 'page_id',
        ],
    ];

    public function getPageIdOptions()
    {
        return Page::all()->lists('title', 'id');
    }

    public function beforeSave()
    {
        $image=$this->image()->withDeferred(post('_session_key'))->first();

        if($image && file_exists($image->getLocalPath())) {
            Storage::disk('media')->put($image->file_name, file_get_contents($image->getLocalPath()));
        }
    }
}
