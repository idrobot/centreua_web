<?php namespace Perevorot\Centr\Models;

use Model;
use Perevorot\Centr\Traits\ModelEvents;

/**
 * Model
 */
class Tender extends Model
{
    use ModelEvents;
    use \October\Rain\Database\Traits\Validation;

    public $implement = [
        '@RainLab.Translate.Behaviors.TranslatableModel',
    ];

    public $translatable = [
        ['name', 'index'=>true],
        ['short', 'index'=>true],
        ['desc', 'index'=>true],
        ['city', 'index'=>true],
    ];

    public $timestamps = false;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required',
        'dt' => 'required',
        'slug' => 'required|unique:perevorot_centr_tenders',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'perevorot_centr_tenders';
}
