<?php namespace Perevorot\Centr\Models;

use Backend\FormWidgets\MediaFinder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Model;
use Perevorot\Centr\Traits\ModelEvents;
use Perevorot\Page\Models\Page;

/**
 * Model
 */
class Article extends Model
{
    use ModelEvents;
    use \October\Rain\Database\Traits\Validation;
    use \Perevorot\Longread\Traits\LongreadMutators;
    use \Perevorot\Longread\Traits\LongreadGenerateThumbs;

    public $implement = [
        '@RainLab.Translate.Behaviors.TranslatableModel',
        '@Perevorot.Longread.Behaviors.LongreadAttachFiles',
    ];

    public $translatable = [
        ['title', 'index'=>true],
        ['desc', 'index'=>true],
        ['short', 'index'=>true],
        ['copyright', 'index'=>true],
    ];
    
    /**
     * @var array Validation rules
     */
    public $rules = [
        'title' => 'required',
        'slug' => 'required|unique:perevorot_centr_articles',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'perevorot_centr_articles';

    public $attachOne = [
        'image' => ['System\Models\File'],
    ];

    public $belongsToMany = [
        'tags' => [
            'Perevorot\Centr\Models\Tag',
            'table'    => 'perevorot_centr_tags_to_articles',
            'key' => 'article_id',
            'otherKey' => 'tag_id',
            'order' => 'name asc',
        ]
    ];

    public $belongsTo = [
        'agroup' => ['Perevorot\Centr\Models\Agroup',
        ],
        'page' => ['Perevorot\Page\Models\Page',
        ],
    ];

    public function getAgroupIdOptions()
    {
        $pages = Agroup::all();
        $pages->push(['id'=>0,'name'=>'не важно']);
        $groups = $pages->sortBy('name')
            ->lists('name', 'id')
        ;

        return $groups;
    }

    public function getPageIdOptions()
    {
        $pages = Page::all();
        $pages->push(['id'=>0,'title'=>'не важно']);
        $pages = $pages->sortBy('title')
            ->lists('title', 'id')
        ;

        return $pages;
    }

    public function beforeSave()
    {
        $image=$this->image()->withDeferred(post('_session_key'))->first();

        if($image && file_exists($image->getLocalPath())) {

            Storage::disk('media')->put($image->file_name, file_get_contents($image->getLocalPath()));

            $image->getThumb(384, 183, [
                'mode' => 'crop',
                'quality' => 90
            ]);
        }
    }
}
