<?php namespace Perevorot\Centr\Models;

use Model;
use October\Rain\Database\Traits\Sortable;
use October\Rain\Database\Traits\Validation;

/**
 * Jury Model
 */
class Jury extends Model
{
    use Sortable, Validation;

    public $implement = [
        '@RainLab.Translate.Behaviors.TranslatableModel',
    ];

    public $translatable = [
        ['full_name', 'index'=>true],
        ['position', 'index'=>true],
        ['company', 'index'=>true],
        ['text', 'index'=>true],
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'perevorot_centr_juries';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'full_name' => 'required',
        'slug' => 'required|unique:perevorot_centr_juries',
        'position' => 'required',
        // 'company' => 'required',
        'text' => 'required',
        'image' => 'required',
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'image' => 'System\Models\File'
    ];
    public $attachMany = [];

    public function getThumbNameAttribute()
    {
        if (!$this->image) {
            return $this->full_name;
        }

        echo "<img src='{$this->image->path}' width='50'><span style='padding: 20px;'>$this->full_name}</span>";
    }
}
