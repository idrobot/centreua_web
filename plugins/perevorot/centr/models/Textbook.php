<?php namespace Perevorot\Centr\Models;

use Model;
use Perevorot\Page\Models\Page;

/**
 * Model
 */
class Textbook extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $implement = [
        '@RainLab.Translate.Behaviors.TranslatableModel',
    ];

    public $translatable = [
        ['name', 'index'=>true],
        ['short', 'index'=>true],
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required',
        'year' => 'required|numeric',
        'sort_order' => 'required',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'perevorot_centr_textbooks';

    public $attachOne = [
        'file' => ['System\Models\File'],
    ];

    public $hasOne = [
        'page' => ['Perevorot\Page\Models\Page',
            'key' => 'id',
            'otherKey' => 'page_id',
        ],
    ];

    public function getPageIdOptions()
    {
        return Page::all()->lists('title', 'id');
    }
}
