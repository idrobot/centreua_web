<?php namespace Perevorot\Centr\Models;

use Model;
use Perevorot\Centr\Traits\ModelEvents;

/**
 * Model
 */
class Group extends Model
{
    use \October\Rain\Database\Traits\Validation, ModelEvents;

    public $implement = [
        '@RainLab.Translate.Behaviors.TranslatableModel',
    ];

    public $translatable = [
        ['name', 'index'=>true],
        ['position', 'index'=>true],
        ['description', 'index'=>true],
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required',
        'sort_order' => 'required',
        'slug' => 'required|unique:perevorot_centr_groups',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'perevorot_centr_groups';

    public function getRegionIdOptions()
    {
        return Region::all()->lists('name', 'id');
    }
}
