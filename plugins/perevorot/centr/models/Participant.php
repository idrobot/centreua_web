<?php namespace Perevorot\Centr\Models;

use Model;
use October\Rain\Database\Traits\Validation;

/**
 * Participant Model
 */
class Participant extends Model
{
    use Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'perevorot_centr_participants';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'full_name' => 'required',
        'birthday' => 'required',
        'work_place' => 'required',
        'position' => 'required',
        'region' => 'required',
        'city' => 'required',
        'email' => 'required|email',
        'phone' => 'required',
        'facebook_link' => 'required|url',
        'about_work' => 'required',
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'contest' => 'Perevorot\Centr\Models\Contest'
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
