<?php namespace Perevorot\Centr\Models;

use Model;
use Str;

/**
 * Model
 */
class Tag extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $implement = [
        '@RainLab.Translate.Behaviors.TranslatableModel',
    ];
    /*
     * Validation
     */
    public $rules = [
    ];

    /*
     * Fillable
     */
    public $fillable = [
        'name'
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'perevorot_centr_tags';

    public $translatable = [
        ['name', 'index'=>true],
    ];

    /**
    * something
    */
    public $belongsToMany = [
        'articles' => [
            'Perevorot\Centr\Models\Article',
            'table'    => 'perevorot_centr_tags_to_articles',
            'key' => 'tag_id',
            'otherKey' => 'article_id',
        ]
    ];

    public function beforeCreate()
    {
        if(empty($this->slug)){
            $this->slug = Str::slug($this->name);
        }
    }
}
