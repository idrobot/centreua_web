<?php namespace Perevorot\Centr\Models;

use Illuminate\Support\Facades\Storage;
use Model;
use Perevorot\Page\Models\Page;

/**
 * Model
 */
class Tool extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $implement = [
        '@RainLab.Translate.Behaviors.TranslatableModel',
    ];
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    public $translatable = [
        ['description', 'index'=>true]
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'sort_order' => 'required',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'perevorot_centr_tools';

    public $attachOne = [
        'image' => ['System\Models\File'],
    ];

    public function getPageIdOptions()
    {
        return Page::all()->lists('title', 'id');
    }

    public function beforeSave()
    {
        $image=$this->image()->withDeferred(post('_session_key'))->first();

        if($image && file_exists($image->getLocalPath())) {

            Storage::disk('media')->put($image->file_name, file_get_contents($image->getLocalPath()));

            $image->getThumb(179, 86, [
                'mode' => 'auto',
                'quality' => 90
            ]);
        }
    }
}
