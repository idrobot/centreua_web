<?php namespace Perevorot\Centr\Models;

use Model;

/**
 * Model
 */
class Subscribe extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
        'email' => 'required',
        'created_at' => 'required',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'perevorot_centr_subscriptions';
}
