<?php namespace Perevorot\Centr\Models;

use Model;
use Perevorot\Centr\Models\Jury;
use Perevorot\Centr\Models\ContestPartner;
use October\Rain\Database\Traits\Validation;
use Perevorot\Longread\Traits\LongreadMutators;
use Perevorot\Longread\Traits\LongreadGenerateThumbs;

/**
 * Contest Model
 */
class Contest extends Model
{
    use Validation, LongreadMutators, LongreadGenerateThumbs;

    public $implement = [
        '@RainLab.Translate.Behaviors.TranslatableModel',
        '@Perevorot.Longread.Behaviors.LongreadAttachFiles',
    ];

    public $translatable = [
        ['name', 'index'=>true],
        // ['items', 'index'=>true],
    ];

    public $jsonable = [
        'items'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'perevorot_centr_contests';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required',
        'slug' => 'required|unique:perevorot_centr_contests',
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function scopeEnabled($query)
    {
        return $query->where($this->table . '.is_enabled', true);
    }

    public function getJuries()
    {
        return Jury::lists('full_name', 'id');
    }

    public function getParners()
    {
        return ContestPartner::lists('name', 'id');
    }

    public function beforeSave()
    {
        if (true == $this->is_enabled) {
            $this->where($this->table . '.id', '<>', $this->id)->update([
                'is_enabled' => false
            ]);
        }
    }

    public function getContests()
    {
        return $this->enabled()->lists('name', 'slug');
    }
}
