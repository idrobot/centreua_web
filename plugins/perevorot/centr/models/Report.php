<?php namespace Perevorot\Centr\Models;

use Model;

/**
 * Model
 */
class Report extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $implement = [
        '@RainLab.Translate.Behaviors.TranslatableModel',
    ];

    public $translatable = [
        ['name', 'index'=>true],
        ['description', 'index'=>true],
        ['year', 'index'=>true],
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'perevorot_centr_reports';

    public $attachOne = [
        'file' => ['System\Models\File'],
        'file_en' => ['System\Models\File'],
    ];
}
