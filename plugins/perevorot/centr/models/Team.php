<?php namespace Perevorot\Centr\Models;

use Illuminate\Support\Facades\Storage;
use Model;
use October\Rain\Database\Traits\Sortable;

/**
 * Model
 */
class Team extends Model
{
    use \October\Rain\Database\Traits\Validation, Sortable;

    public $implement = [
        '@RainLab.Translate.Behaviors.TranslatableModel',
    ];

    public $translatable = [
        ['full_name', 'index'=>true],
        ['description', 'index'=>true],
        ['position', 'index'=>true],
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
        'full_name' => 'required',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'perevorot_centr_teams';

    public $attachOne = [
        'image' => ['System\Models\File'],
    ];

    public $belongsTo = [
        'group' => ['Perevorot\Centr\Models\Group'],
    ];

    public function getRegionIdOptions()
    {
        $data = Region::all();
        $data->push(['id'=>0,'name'=>'не важно']);
        $data = $data->lists('name', 'id');

        return $data;
    }

    public function getGroupIdOptions()
    {
        return Group::all()->lists('name', 'id');
    }

    public function beforeSave()
    {
        $image=$this->image()->withDeferred(post('_session_key'))->first();

        if($image && file_exists($image->getLocalPath())) {

            Storage::disk('media')->put($image->file_name, file_get_contents($image->getLocalPath()));

            $image->getThumb(352, 352, [
                'mode' => 'crop',
                'quality' => 90
            ]);
        }
    }
}
