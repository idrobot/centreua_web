<?php namespace Perevorot\Centr\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ThumbsGenerate extends Command
{
    protected $name = 'thumbs:generate';

    protected $description = '';

    public function handle()
    {
        foreach(['partner', 'article', 'team', 'tool'] as $modelName) {
            $modelName='Perevorot\Centr\Models\\'.ucfirst($modelName);
            $model=new $modelName;

            $data=$model->get();

            foreach($data as $item){
                $item->save();
            }

            $this->output->writeln($modelName.': '.sizeof($data));
            
            sleep(1);
        }

        foreach(['page'] as $modelName) {
            $modelName='Perevorot\Page\Models\\'.ucfirst($modelName);
            $model=new $modelName;

            $data=$model->get();

            foreach($data as $item){
                $item->save();
            }

            $this->output->writeln($modelName.': '.sizeof($data));

            sleep(1);
        }
    }

    protected function getArguments()
    {
        return [];
    }

    protected function getOptions()
    {
        return [];
    }

}