<?php

namespace Perevorot\Settings\Models;

use Model;

/**
 * Model
 */
class Common extends Model
{
    use \Perevorot\Page\Traits\CacheClear;

    public $implement = [
        'System.Behaviors.SettingsModel',
        'RainLab.Translate.Behaviors.TranslatableModel',
    ];

    public $translatable = [
        'address',
        'work_time',
        'copyright',
        '404_text',
        'donate_title',
        'donate_text',
        'donate_form_title',
        'donate_requisites',
        'donate_requisite_name',
    ];

    public $settingsCode = 'common';

    public $settingsFields = 'fields.yaml';

    public $attachOne = [
        'logo' => 'System\Models\File',
        'mobile_logo' => 'System\Models\File',
        'donate_image' => 'System\Models\File',
        'donate_form_image' => 'System\Models\File',
    ];
}
