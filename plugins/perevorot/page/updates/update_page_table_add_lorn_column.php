<?php namespace Perevorot\Page\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdatePageTableAddLornColumn extends Migration
{
    public function up()
    {
        if (Schema::hasColumns('perevorot_page_page', ['is_lorn'])) {
            return;
        }

        Schema::table('perevorot_page_page', function($table)
        {
            $table->boolean('is_lorn')->default(false);
        });
    }

    public function down()
    {
        if (Schema::hasTable('perevorot_page_page')) {
            Schema::table('perevorot_page_page', function($table)
            {
                $table->dropColumn('is_lorn');
            });
        }
    }
}
