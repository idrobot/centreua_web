<?php namespace Perevorot\Page\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdatePageTableAddAttachPage extends Migration
{
    public function up()
    {
        Schema::table('perevorot_page_page', function($table)
        {
            $table->boolean('is_attach')->default(false);
            $table->integer('attach_id')->nullable()->unsigned();
        });
    }

    public function down()
    {
        Schema::table('perevorot_page_page', function($table)
        {
            $table->dropColumn([
                'is_attach',
                'attach_id',
            ]);
        });
    }
}
